from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep

## Signup test
def Signup(driver):
    # login page
    click_signup = driver.find_element_by_class_name('sig_link').click()
    # signup page
    driver.implicitly_wait(10)
    sign_name = driver.find_element_by_id('formBasicName').send_keys('e2etest_12/15_16_36')
    sign_email = driver.find_element_by_id('formBasicEmail').send_keys('e2etest_12/15_16_36@example.com')
    sign_password = driver.find_element_by_id('formBasicPassword').send_keys('1234')
    sign_btn = driver.find_element_by_class_name('submit_btn').click()
    # home page
    sleep(2)
    locator = (By.CLASS_NAME, 'title')
    WebDriverWait(driver, 20).until(EC.presence_of_element_located(locator))
    assert ("Login" in driver.find_element_by_class_name('title').text), "Signup unsucessfully"
    print("Signup sucessfully")

## Signout test
def Signout(driver):
    dropdown = driver.find_element_by_class_name('dropdown-toggle').click()
    sign_out = driver.find_element_by_id('signout').click()
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'title')))
    assert ("Login" in driver.find_element_by_class_name('title').text), "Signout unsucessfully"
    print("Signout sucessfully")

## Login test
def Login(driver):
    # Login page
    login_email = driver.find_element_by_id('formBasicEmail').send_keys('e2etest_12/15_16_36@example.com')
    login_password = driver.find_element_by_id('formBasicPassword').send_keys('1234')
    login_btn = driver.find_element_by_class_name('submit_btn').click()
    # home page
    locator = (By.CLASS_NAME, 'name')
    WebDriverWait(driver, 10).until(EC.presence_of_element_located(locator))
    assert ("All Activities" in driver.find_element_by_class_name('name').text), "Login unsucessfully"
    print("Login sucessfully")

## Create an meeting
def Create(driver, name):
    origin_meetings = len(driver.find_elements_by_class_name('act'))

    host = driver.find_element_by_class_name('bnt_host').click()
    # host page
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'bnt_create')))
    create = driver.find_element_by_class_name('bnt_create').click()
    # create page
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'formBasicName')))
    meeting_name = driver.find_element_by_id('formBasicName').send_keys(name)
    meeting_description = driver.find_element_by_id('formBasicDescription').send_keys('this is a description')
    meeting_location = driver.find_element_by_id('formBasicLocation').send_keys('EC122')
    meeting_link = driver.find_element_by_id('formBasicLink').send_keys('www.google.com')
    meeting_start = driver.find_element_by_id('formStart').send_keys('8')
    meeting_end = driver.find_element_by_id('formGridEnd').send_keys('20')
    submit = driver.find_element_by_class_name('submit_btn').click()
    # home page
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'act')))
    new_meetings = len(driver.find_elements_by_class_name('act'))
    if new_meetings == origin_meetings + 1:
        print("Create sucessfully")
    else:
        print("Create unsucessfully") 


if __name__ == '__main__':
    # open webdriver
    driver = webdriver.Chrome('./chromedriver')
    wait = WebDriverWait(driver, 10)
    #driver.get('http://meetingelf.com')
    driver.get('http://meetingelf.com:8080/site')

    # test
    Signup(driver)
    #Signout(driver)
    Login(driver)
    Create(driver, "E2E2")
    Signout(driver)

    # close webdriver
    driver.close()
