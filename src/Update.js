import React from 'react';
import NavBar from './component/NavBar';
import './component/css/basic.css';
import { Row, Col, Form, Container, Button } from 'react-bootstrap';
import path from './config';

class Update extends React.Component {
	constructor(props) {
		// meet_id = this.props.location.state.meet_id
		super(props);
		this.state = {
			title: "",
			description: "",
			meeting_link: "",
			location: "",
			start_hour: "",
			end_hour: "",
			host_id: ""
		}
		this.SendData = this.SendData.bind(this);


		const requestOptions = {
			method: 'GET',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
		};
		fetch(path.url + '/meetings/' + this.props.match.params.hash_id, requestOptions)
			.then(response => response.json())
			.then(data => {
				// console.log(data);
				this.setState({
					title: data.title,
					description: data.description,
					meeting_link: data.meeting_link,
					location: data.location,
					start_hour: data.start_hour,
					end_hour: data.end_hour
				})
			})
		/*
		fetch(path.url + '/users/' + this.props.location.state.user_id + '/hosts/?hash_id=' + this.props.location.state.meet_id, requestOptions)
			.then(response => response.json())
			.then(data => {
				this.setState({
					host_id: data[0].id
				})
			})
		*/
	}
	SendData() {
		// backend
		const requestOptions = {
			method: 'PATCH',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
			body: JSON.stringify({
				// "token": localStorage.getItem('user_id'),
				"title": this.state.title,
				"description": this.state.description,
				"meeting_link": this.state.meeting_link,
				"location": this.state.location,
				"start_hour": this.state.start_hour,
				"end_hour": this.state.end_hour
			})
		};

		fetch(path.url + '/meetings/' + this.props.match.params.hash_id, requestOptions)
			.then(response => response.json())
			.then(data => {
				// console.log(data);
				this.props.history.push({
					pathname: '/home'
				})
			});
	}

	render() {
		return (
			<div>
				<NavBar history={this.props.history} />
				<h1 className="page_title"> Update an Activity</h1>
				<Container>
					<Row>
						<Col>
							<Form>
								<Form.Group controlId="formBasicName" onChange={e => this.setState({ title: e.target.value })}>
									<Form.Label className="form_text">Name</Form.Label>
									<Form.Control type="name" className="form_input" defaultValue={this.state.title} />
								</Form.Group>
								<Form.Group controlId="formBasicDescription" onChange={e => this.setState({ description: e.target.value })}>
									<Form.Label className="form_text">Description</Form.Label>
									<Form.Control className="form_input" as="textarea" rows={6} defaultValue={this.state.description} />
								</Form.Group>
								<Form.Group controlId="formBasicLocation" onChange={e => this.setState({ location: e.target.value })}>
									<Form.Label className="form_text">Location</Form.Label>
									<Form.Control type="location" className="form_input" defaultValue={this.state.location} />
								</Form.Group>
								<Form.Group controlId="formBasicLink" onChange={e => this.setState({ meeting_link: e.target.value })}>
									<Form.Label className="form_text">Meeting Link</Form.Label>
									<Form.Control type="link" className="form_input" defaultValue={this.state.meeting_link} />
								</Form.Group>
							</Form>
						</Col>
						<Col>
							<Form.Row>
								<Form.Group as={Col} controlId="formStart" onChange={e => this.setState({ start_hour: e.target.value })}>
									<Form.Label className="form_text">Start hour</Form.Label>
									<Form.Control type="start" className="form_input" defaultValue={this.state.start_hour} />
								</Form.Group>

								<Form.Group as={Col} controlId="formGridEnd" onChange={e => this.setState({ end_hour: e.target.value })}>
									<Form.Label className="form_text">End hour</Form.Label>
									<Form.Control type="end" className="form_input" defaultValue={this.state.end_hour} />
								</Form.Group>
							</Form.Row>
							<div className="div_btn_right">
								<Button variant="primary" className="submit_btn" onClick={this.SendData}>Update</Button>{' '}
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Update;