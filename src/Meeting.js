import React from 'react';
import NavBar from './component/NavBar';
import CalenderMeet from './component/CalenderMeet';
import ReferenceList from './component/ReferenceList';
import { Row, Col, Container } from 'react-bootstrap';
import path from './config';
import './component/css/basic.css';

class Meeting extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			title: "name",
			description: "hello",
			location: "EC122",
			meeting_link: "www.google.com",
			final_slots: [],
			loaded: false
		}

		const requestOptions = {
			method: 'GET',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
		};

		fetch(path.url + '/meetings/' + this.props.match.params.hash_id , requestOptions)
			.then(response => response.json())
			.then(data => {
				var temp = []
				// backend
				for(var i = 0;i < data.final_slots.length;i++){
					temp.push(data.final_slots[i]);
				}
				this.setState({
					title: data.title,
					description: data.description,
					location: data.location,
					meeting_link: data.meeting_link,
					final_slots: temp,
					loaded: true
				});
			});
	}

	render(){
		if(this.state.loaded){
			return(
				<div>
				  <NavBar  history = {this.props.history} />
				  <h1 className="page_title">{this.state.title}</h1>
				  <h6 className = "description">{this.state.description}</h6>
				  <Container>
					  <Row>
					    <Col>
				    		<h5 className = "context">location : {this.state.location}</h5>
				    		<h5 className = "context">meeting link : {this.state.meeting_link}</h5>
				    		<ReferenceList hash_id = {this.props.match.params.hash_id} history = {this.props.history} />
					    </Col>
					    <Col>
					    	<CalenderMeet final_slots = {this.state.final_slots} />
					    </Col>
					  </Row>
				  </Container>
				</div>
			);
		}else{
			return null;
		}	
	}
}

export default Meeting;