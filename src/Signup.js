import React from 'react';
import { Navbar, Button, Form, Container, Row, Col } from 'react-bootstrap';
import './component/css/basic.css';
import path from './config';

class Signup extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "default",
			email: "default@example.com",
			password: "defaultpassword"
		}
		this.SendData = this.SendData.bind(this);
	}

	SendData() {
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
			body: JSON.stringify({
				"email": this.state.email,
				"password": this.state.password,
				"username": this.state.name
			})
		};

		fetch(path.url + '/users', requestOptions)
			.then(response => response.json())
			.then(data => {
				// console.log(data);
				this.props.history.push({
					pathname: '/'
				})
			});
	}

	render() {
		return (
			<div>
				<Navbar expand="lg" className="bar">
					<Navbar.Brand className="brand" href="/">Meetingelf</Navbar.Brand>
				</Navbar>
				<Container className="con">
					<Row>
						<Col></Col>
						<Col xs={6}>
							<h1 className="title">Sign up</h1>
							<Form>
								<Form.Group controlId="formBasicName" onChange={e => this.setState({ name: e.target.value })}>
									<Form.Label className="form_text">Name</Form.Label>
									<Form.Control type="name" placeholder="Enter Name" className="form_input" />
								</Form.Group>

								<Form.Group controlId="formBasicEmail" onChange={e => this.setState({ email: e.target.value })}>
									<Form.Label className="form_text">Email address</Form.Label>
									<Form.Control type="email" placeholder="Enter email" className="form_input" />
								</Form.Group>

								<Form.Group controlId="formBasicPassword" onChange={e => this.setState({ password: e.target.value })}>
									<Form.Label className="form_text">Password</Form.Label>
									<Form.Control type="password" placeholder="Enter Password" className="form_input" />
								</Form.Group>
								<div className="div_btn">
									<Button variant="primary" className="submit_btn" onClick={this.SendData}>Sign up</Button>{' '}
								</div>
							</Form>
						</Col>
						<Col></Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Signup;
