import React from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import { Plus } from 'react-bootstrap-icons';
import Reference from './Reference';
import path from '../config';
import './css/basic.css';

class ReferenceList extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			reference_title: "google",
			reference_link: "www.google.com",
			references: [],
			show: false,
			setshow: false
		}

		const requestOptions = {
	      method: 'GET',
	      headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
	    };
	    fetch(path.url + '/meetings/' + this.props.hash_id + '/references', requestOptions)
	        .then(response => response.json())
	        .then(data => {
	        	var temp = [];
	        	for (var i = 0; i < data.length; i++) {
					temp.push(<Reference id = {data[i].id} title = {data[i].title} link = {data[i].link} history = {this.props.history} />);
				}
			    this.setState({references: temp});
	        });


		this.addReference = this.addReference.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.handleShow = this.handleShow.bind(this);
	}

	addReference(){
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json'
			},
			body: JSON.stringify({
				"title": this.state.reference_title,
				"link": this.state.reference_link
			})
		};

		fetch(path.url + '/meetings/' + this.props.hash_id + '/references', requestOptions)
			.then(response => response.json())
			.then(data => {
				console.log(data);
				this.props.history.go(0);
			});
		this.handleClose();
	}

	handleClose(){
		this.setState(state => ({
      		show: false,
      		setshow: false
    	}));
	}

	handleShow(){
		this.setState(state => ({
      		show: true,
      		setshow: true
    	}));
	}

	render(){
		return(
			<div>
				<span className = "name">
					reference :   
					<Button variant="warning" className = "bnt_create" onClick={this.handleShow}>
						<Plus size={32} />
					</Button>{' '}
				</span>
				<div className = "reference_block">
					{this.state.references}
				</div>
				<Modal animation={false} show={this.state.show} onHide={this.handleClose} className = "modal">
			        <Modal.Header closeButton>
			          <Modal.Title>Add Reference</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			        	<Form>
							<Form.Group controlId="formBasicText" onChange={e => this.setState({ reference_title: e.target.value })}>
								<Form.Label className="form_text">Reference Title</Form.Label>
								<Form.Control type="text" className="form_input" />
							</Form.Group>
							<Form.Group controlId="formBasicLink" onChange={e => this.setState({ reference_link: e.target.value })}>
								<Form.Label className="form_text">Reference Link</Form.Label>
								<Form.Control type="link"className="form_input" />
							</Form.Group>
						</Form>
			        </Modal.Body>
			        <Modal.Footer>
			          <Button variant="secondary" onClick={this.handleClose} className = "modal_no">
			            Cancel
			          </Button>
			          <Button variant="primary" onClick={this.addReference} className = "modal_yes">
			            Add
			          </Button>
			        </Modal.Footer>
			    </Modal>
			</div>
		);
	}
}

export default ReferenceList;