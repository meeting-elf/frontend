import React from 'react';
import { Button, Dropdown, ButtonGroup, Modal } from 'react-bootstrap';
import { TrashFill } from 'react-bootstrap-icons';
import path from '../config';
import './css/basic.css';

class Reference extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			id: props.id,
			title: props.title,
			link: props.link,
			show: false,
			setshow: false
		}

		this.goDelete = this.goDelete.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.handleShow = this.handleShow.bind(this);
	}

	goDelete(){
		const requestOptions = {
	      		method: 'DELETE',
	      		headers: { 'Content-Type': 'application/json' 
					//'Accept': '*/*'
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			}
			// credentials: 'include',
      		// withCredentials: true,
	    };
	    fetch(path.url + '/references/' + this.state.id, requestOptions)
	        .then(response => response.json())
	        .then(data => {
	        	console.log(data);
				this.props.history.go(0);
	        });
	}

	handleClose(){
		this.setState(state => ({
      		show: false,
      		setshow: false
    	}));
	}

	handleShow(){
		this.setState(state => ({
      		show: true,
      		setshow: true
    	}));
	}

	render(){
		var link = this.state.link;
		return(
			<div>
				<Dropdown as={ButtonGroup} style = {{width: "100%"}} >
  				<Button variant="info" onClick={()=> window.open(link, "_blank")} className = "reference" block>
  					{this.state.title}
  				</Button>
  				<Dropdown.Toggle split variant="info" id="dropdown-split-basic" className = "reference" />
				<Dropdown.Menu>
					<Dropdown.Item onClick={this.handleShow}>
						<TrashFill className = "dropdown_icon" />
						delete
					</Dropdown.Item>
  				</Dropdown.Menu>
				</Dropdown>
				<Modal animation={false} show={this.state.show} onHide={this.handleClose} className = "modal">
			        <Modal.Header closeButton>
			          <Modal.Title>Do You Want to Delete this Reference?</Modal.Title>
			        </Modal.Header>
			        <Modal.Footer>
			          <Button variant="secondary" onClick={this.handleClose} className = "modal_no">
			            No
			          </Button>
			          <Button variant="primary" onClick={this.goDelete} className = "modal_yes">
			            Yes
			          </Button>
			        </Modal.Footer>
			    </Modal>
			</div>			
		)
		
	}
}
export default Reference;