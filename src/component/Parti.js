import React from 'react';
import Act from './Act';
import path from '../config';
import './css/basic.css';

class Parti extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			acti: []
		}

	    const requestOptions = {
	      method: 'GET',
	      headers: { 'Content-Type': 'application/json', 
	      			 'Accept': 'application/json',
	      			 // 'Accept-Control-Allow-Origin': path.url,
                 	 // 'Accept-Control-Allow-Credentials': true,
	      },
	      // credentials: 'include',
          // withCredentials: true,
	    };
	    fetch(path.url + '/users/' + localStorage.getItem('user_id') + '/meetings', requestOptions)
	        .then(response => response.json())
	        .then(data => {
	        	var temp = [];
	        	for (var i = 0; i < data.length; i++) {
	        		var voted = false;
	        		if(data[i]['meeting'].final_slots.length !== 0){
	        			voted = !voted;
	        		}
					temp.push(<Act key = {data[i]['meeting'].hash_id} meet_id = {data[i]['meeting'].hash_id} title = {data[i]['meeting'].title} 
								voted = {voted} history = {this.props.history} />)
				}
				this.setState({ acti: temp })
	        });
	}

	render(){
		return(
				<div className = "list">
					{this.state.acti}
				</div>
		);
	}
}

export default Parti;
