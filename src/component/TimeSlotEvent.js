import React from 'react';
import './css/basic.css';
import { table } from 'react-bootstrap';

const active_style = {
    background: "#ED9E50"
}

const inactive_style = {
}

class TimeSlot extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chosen: props.chosen,
            title: props.title
        }
        // this.change_chosen = this.change_chosen.bind(this);
    }

    // change_chosen() {
    //     this.setState({
    //         chosen: !this.state.chosen
    //     })           
    // }

    render() {
        var style;
        if (this.state.chosen)
            style = active_style;
        else
            style = inactive_style;

        return (<td style={style} >{this.state.title}</td>)

    }
}

export default TimeSlot;