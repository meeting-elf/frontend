import React from 'react';
import './css/basic.css';
import { table } from 'react-bootstrap';



class TimeSlot extends React.Component {
    constructor(props) {
        super(props);
        var max_vote = Math.max(1, props.max_vote)
        this.state = {
            num: props.num,
            max_vote: max_vote,
            active_style: {
                background: "#ED9E50",
                opacity: props.num / max_vote
            },
            inactive_style: {
            }
        }

        // const active_style = {
        //     background: "#8CBDB9"
        // }

        // const inactive_style = {
        // }
        // this.change_chosen = this.change_chosen.bind(this);
    }

    // change_chosen() {
    //     if(this.props.click){
    //         this.setState({
    //             chosen: !this.state.chosen
    //         })           
    //     }
    // }

    render() {
        var style;
        if (this.state.num != 0)
            style = this.state.active_style;
        else
            style = this.state.inactive_style;

        return (<td style={style}> </td>)

    }
}

export default TimeSlot;