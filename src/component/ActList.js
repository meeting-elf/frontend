import React from 'react';
import Parti from './Parti';
import Host from './Host';
import { Button } from 'react-bootstrap';
import { PeopleFill, PersonFill, Plus } from 'react-bootstrap-icons';
import './css/basic.css';

// props : user_id, history

class ActList extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			mode: true,
		}

		this.handleClick = this.handleClick.bind(this);
		this.goCreate = this.goCreate.bind(this);
	}

	handleClick(){
		this.setState(state => ({
      		mode: !state.mode
    	}));
	}

	goCreate(){
		this.props.history.push({
				pathname:'/create'
		})
	}
	
	render(){
		return(
			<div className = "block">
				<span className = "name">
					{this.state.mode ? 'All Activities' : 'My Hosts'}
					<Button variant="info" className = "bnt_host" onClick={this.handleClick}>
						{this.state.mode ? <PersonFill size={25} /> : <PeopleFill size={25} />}
					</Button>{' '}
					<Button variant="warning" className = "bnt_create" style = {{visibility: this.state.mode ? "hidden" : "visible"}} onClick={this.goCreate}>
						<Plus size={35} />
					</Button>{' '}
				</span>
					{this.state.mode ? <Parti history = {this.props.history} /> : <Host history = {this.props.history} />}
			</div>
		);
	}
}


export default ActList;
