import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { PersonSquare, HouseFill, BoxArrowRight } from 'react-bootstrap-icons';
import './css/basic.css';
import path from '../config';

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "egg",
      signin: false
    }

    this.goHome = this.goHome.bind(this);
    this.Signout = this.Signout.bind(this);
    this.Signin = this.Signin.bind(this);

    const req = {
	method: 'GET',
      	headers: { 'Content-Type': 'application/json', 
                   'Accept': 'application/json',
                 // 'Accept-Control-Allow-Origin': path.url,
                 // 'Accept-Control-Allow-Credentials': true,
      },
    };
    fetch(path.url + '/auth/status', req)
        .then(response => {
		response.json();
		if (response.status == 200) {	
	    	    const requestOptions = {
      		        method: 'GET',
      		        headers: { 'Content-Type': 'application/json', 
                 	           'Accept': 'application/json',
      		        },
    	            };
    	            fetch(path.url + '/users/' + localStorage.getItem('user_id'), requestOptions)
        	        .then(response => response.json())
        	        .then(data2 => {
          		    // console.log(data);
          		    this.setState({ 
			        username: data2.username,
			        signin: true 
			    })
                    });
		}
	})
        .then(data => {});
      
  }

  goHome() {
    this.props.history.push({
      pathname: '/home'
    })
  }

  Signin(){
    this.props.history.push({
      pathname:'/'
    })
  }

  Signout(){
    localStorage.clear();
    const requestOptions = {
          method: 'DELETE',
          headers: { 
            'Content-Type': 'application/json'
          },
          // credentials: 'include',
          // withCredentials: true
        };

      fetch(path.url + '/auth/logout', requestOptions)
          .then(response => response.json())
          .then(data => {
            this.props.history.push({
            pathname:'/'
          })
      });
  }

  render() {
    const isSignin = this.state.signin;
    return (
      <Navbar expand="lg" className="bar">
        <Navbar.Brand className="brand" >Meetingelf</Navbar.Brand>
        <Nav className="ml-auto">
          <NavDropdown
            alignRight
            title={<div style={{ display: 'inline-block' }}><PersonSquare size={32} /></div>}
            id="basic-nav-dropdwon"
          >
            {isSignin ?
            <NavDropdown.Item disabled>Signed in as {this.state.username}</NavDropdown.Item>
            :
            <NavDropdown.Item disabled>Please signin to participate</NavDropdown.Item>
            }
            {isSignin ?
            <NavDropdown.Item onClick={this.goHome} id="gohome">
              <HouseFill className="dropdown_icon" />
              home
            </NavDropdown.Item>
            :
            null
            }
            <NavDropdown.Divider />
            {isSignin ?
            <NavDropdown.Item onClick={this.Signout} id="signout">
              <BoxArrowRight className="dropdown_icon" />
              sign out
            </NavDropdown.Item>
            :
            <NavDropdown.Item onClick={this.Signin} id="signin">
              <BoxArrowRight className="dropdown_icon" />
              sign in
            </NavDropdown.Item>
            }
          </NavDropdown>
        </Nav>
      </Navbar>
    );      
  }
}

export default NavBar;
