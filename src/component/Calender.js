import React from 'react';
import './css/basic.css';
import TimeSlot from './TimeSlotAvailability';
// import path from '../config';
import { Container, Row, Col, table } from 'react-bootstrap';

class Calender extends React.Component {
	constructor(props) {
		super(props);
		// user_id = tdis.props.user_id
		this.state = {
			voted_slots: props.vote_slots,
			max_vote: props.max_vote,
			vote_slots: [],
			vote_slots_display: [],
			vote_slots_availability: []
			// render: false
		}
		// console.log(this.state.voted_slots);
		// this.vote_chosen = []
		// this.vote_slots = [];
		// this.vote_slots_display = [];
		// this.vote_slots_availability = [];

		for (var i = 0; i < 22 - 8 + 1; i++) {
			// this.vote_slots.push([]);
			this.state.vote_slots_display.push([]);
			this.state.vote_slots_availability.push([]);

			for (var j = 0; j < 7; j++) {
				// this.vote_slots[i].push(React.createRef());
				// this.vote_slots_display[i].push(<TimeSlot chosen={false} ref={this.vote_slots[i][j]} />);
				this.state.vote_slots_availability[i].push(0);
			}
		}

		var col = 0;
    	var row = 0;
    	// console.log(this.state.voted_slots);
    	for(var i = 0; i < this.state.voted_slots.length; i++){
			col = Number(this.state.voted_slots.[i].day)-1;
			row = Number(this.state.voted_slots.[i].hour)-8;
			this.state.vote_slots_availability[row][col] = this.state.voted_slots[i].count;
    	}
    	// console.log(this.state.vote_slots_availability);
    	for (var i = 0; i < 22 - 8 + 1; i++) {
			for (var j = 0; j < 7; j++) {
				this.state.vote_slots_display[i].push(<TimeSlot num = {this.state.vote_slots_availability[i][j]} max_vote = {this.state.max_vote} />);
			}
		}

		// console.log(this.state.vote_slots_display);
	}

	render() {
			return (
				<div className="calender">

					<table class="table table-bordered table-hover table-width-fixed">
						<thead>
							<tr className="calender_top">
								<th></th>
								<th scope="col">Mon</th>
								<th scope="col">Tue</th>
								<th scope="col">Wed</th>
								<th scope="col">Thu</th>
								<th scope="col">Fri</th>
								<th scope="col">Sat</th>
								<th scope="col">Sun</th>
							</tr>
						</thead>
						<tbody className="calender_text">
							<tr>
								<td>08</td>
								{this.state.vote_slots_display[0]}
							</tr>
							<tr>
								<td>09</td>
								{this.state.vote_slots_display[1]}
							</tr>
							<tr>
								<td>10</td>
								{this.state.vote_slots_display[2]}
							</tr>
							<tr>
								<td>11</td>
								{this.state.vote_slots_display[3]}
							</tr>
							<tr>
								<td>12</td>
								{this.state.vote_slots_display[4]}
							</tr>
							<tr>
								<td>13</td>
								{this.state.vote_slots_display[5]}
							</tr>
							<tr>
								<td>14</td>
								{this.state.vote_slots_display[6]}
							</tr>
							<tr>
								<td>15</td>
								{this.state.vote_slots_display[7]}
							</tr>
							<tr>
								<td>16</td>
								{this.state.vote_slots_display[8]}
							</tr>
							<tr>
								<td>17</td>
								{this.state.vote_slots_display[9]}
							</tr>
							<tr>
								<td>18</td>
								{this.state.vote_slots_display[10]}
							</tr>
							<tr>
								<td>19</td>
								{this.state.vote_slots_display[11]}
							</tr>
							<tr>
								<td>20</td>
								{this.state.vote_slots_display[12]}
							</tr>
							<tr>
								<td>21</td>
								{this.state.vote_slots_display[13]}
							</tr>
							<tr>
								<td>22</td>
								{this.state.vote_slots_display[14]}
							</tr>
						</tbody>
					</table>

				</div>
			)
	}
}

export default Calender;