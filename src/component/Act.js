import React from 'react';
import { Button} from 'react-bootstrap';
import './css/basic.css';


class Act extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			meet_id: props.meet_id,
			title: props.title,
			voted: props.voted
		}
		this.goMeeting = this.goMeeting.bind(this);
	}

	goMeeting(){
		// voting
		if(!this.state.voted){
			this.props.history.push({
				pathname:"/meeting/" + this.state.meet_id + "/vote",
			})
		}else{
			this.props.history.push({
				pathname:"/meeting/" + this.state.meet_id,
			})
		}
	}

	render(){
		return(
			<div>
				<Button variant="info" onClick={this.goMeeting} style = {{backgroundColor : this.state.voted ? "#ED9E50": "#8CBDB9"}} className = "act" block>
					{this.state.title}
				</Button>{' '}
			</div>
		);
	}
}

export default Act;