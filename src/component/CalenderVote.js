import React from 'react';
import './css/basic.css';
import TimeSlot from './TimeSlotVote';
import path from '../config';
import { Container, Row, Col, table, Button } from 'react-bootstrap';

class CalenderVote extends React.Component {
	constructor(props) {
		super(props);
		// user_id = tdis.props.user_id
		this.state = {
			mode: props.mode,
			voted_slots: props.voted_slots,
			vote_slots: [],
			vote_slots_display: []
		}

		// this.vote_slots = [];
		// this.vote_slots_display = [];

		console.log(props.voted_slots);
		console.log(this.state.voted_slots);

		this.SendData = this.SendData.bind(this);

		for (var i = 0; i < 22 - 8 + 1; i++) {
			this.state.vote_slots.push([]);
			this.state.vote_slots_display.push([]);

			for (var j = 0; j < 7; j++) {
				this.state.vote_slots[i].push(React.createRef());
				this.state.vote_slots_display[i].push(<TimeSlot chosen={false} ref={this.state.vote_slots[i][j]} />);
			}
		}

		var col = 0;
    	var row = 0;
    	for(var i = 0; i < this.state.voted_slots.length; i++){
			col = Number(this.state.voted_slots[i].day)-1;
			row = Number(this.state.voted_slots[i].hour)-8;
			this.state.vote_slots_display[row][col] = <TimeSlot chosen={true} ref={this.state.vote_slots[row][col]} />;
    	}
		// this.vote_slots[0][0].current.chosen

	}

	SendData(){
		var vote_slots = [];
		// console.log(this.state.vote_slots[0][0].current.state.chosen);
		for(var i = 0; i < 22 - 8 + 1; i++){
			for(var j = 0; j < 7; j++){
				if(this.state.vote_slots[i][j].current.state.chosen){
					vote_slots.push({
						"day": j + 1,
						"hour": i + 8,
						"minute": 0,
        				"date": null
					})
				}
			}
		}
		// console.log(vote_slots);

		if(this.state.mode == "vote"){
			const requestOptions = {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', 
						   'Accept': 'application/json'
				},
				body: JSON.stringify({
				    "vote_slots": vote_slots
				})
			};

			fetch(path.url + '/meetings/' + this.props.hash_id + '/vote', requestOptions)
				.then(response => response.json())
				.then(data => {
					// console.log(data);
					this.props.history.push({
						pathname: '/home'
					})
				});
		}else{
			const requestOptions = {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', 
						   'Accept': 'application/json'
				},
				body: JSON.stringify({
				    "final_slots": vote_slots
				})
			};

			fetch(path.url + '/meetings/' + this.props.hash_id + '/final_vote', requestOptions)
				.then(response => response.json())
				.then(data => {
					// console.log(data);
					this.props.history.push({
						pathname: '/home'
					})
				});
		}
	}

	render() {
		return (
			<div className="calender">

				<table class="table table-bordered table-hover table-width-fixed">
					<thead>
						<tr className="calender_top">
							<th></th>
							<th scope="col">Mon</th>
							<th scope="col">Tue</th>
							<th scope="col">Wed</th>
							<th scope="col">Thu</th>
							<th scope="col">Fri</th>
							<th scope="col">Sat</th>
							<th scope="col">Sun</th>
						</tr>
					</thead>
					<tbody className="calender_text">
						<tr>
							<td>08</td>
							{this.state.vote_slots_display[0]}
						</tr>
						<tr>
							<td>09</td>
							{this.state.vote_slots_display[1]}
						</tr>
						<tr>
							<td>10</td>
							{this.state.vote_slots_display[2]}
						</tr>
						<tr>
							<td>11</td>
							{this.state.vote_slots_display[3]}
						</tr>
						<tr>
							<td>12</td>
							{this.state.vote_slots_display[4]}
						</tr>
						<tr>
							<td>13</td>
							{this.state.vote_slots_display[5]}
						</tr>
						<tr>
							<td>14</td>
							{this.state.vote_slots_display[6]}
						</tr>
						<tr>
							<td>15</td>
							{this.state.vote_slots_display[7]}
						</tr>
						<tr>
							<td>16</td>
							{this.state.vote_slots_display[8]}
						</tr>
						<tr>
							<td>17</td>
							{this.state.vote_slots_display[9]}
						</tr>
						<tr>
							<td>18</td>
							{this.state.vote_slots_display[10]}
						</tr>
						<tr>
							<td>19</td>
							{this.state.vote_slots_display[11]}
						</tr>
						<tr>
							<td>20</td>
							{this.state.vote_slots_display[12]}
						</tr>
						<tr>
							<td>21</td>
							{this.state.vote_slots_display[13]}
						</tr>
						<tr>
							<td>22</td>
							{this.state.vote_slots_display[14]}
						</tr>
					</tbody>
				</table>
				<div className="div_btn">
					<Button variant="primary" className="submit_btn" onClick={this.SendData}>Submit</Button>{' '}
				</div>
			</div>
		)
	}
}

export default CalenderVote;
