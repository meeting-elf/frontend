import React from 'react';
import { Button, Dropdown, ButtonGroup, Modal } from 'react-bootstrap';
import { GearFill, Tools, TrashFill } from 'react-bootstrap-icons';
import path from '../config';
import './css/basic.css';


class ActEdit extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			meet_id: props.meet_id,
			title: props.title,
			voted: props.voted,
			show: false,
			setshow: false
		}
		this.goMeeting = this.goMeeting.bind(this);
		this.goUpdate = this.goUpdate.bind(this);
		this.goDecision = this.goDecision.bind(this);
		this.goDelete = this.goDelete.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.handleShow = this.handleShow.bind(this);
	}

	goMeeting(){
		// voting
		if(!this.state.voted){
			this.props.history.push({
				pathname:"/meeting/" + this.state.meet_id + "/vote"
			})
		}else{
			this.props.history.push({
				pathname:"/meeting/" + this.state.meet_id
			})
		}
	}

	goUpdate(){
		this.props.history.push({
				pathname: '/meeting/' + this.state.meet_id + '/update'
		})
	}

	goDecision(){
		this.props.history.push({
				pathname: '/meeting/' + this.state.meet_id + '/decision'
		})		
	}

	goDelete(){
		// backend
		const requestOptions = {
	      		method: 'DELETE',
	      		headers: { 'Content-Type': 'application/json' 
					//'Accept': '*/*'
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			}
			// credentials: 'include',
      		// withCredentials: true,
	    };
	    fetch(path.url + '/meetings/' + this.state.meet_id, requestOptions)
	        .then(response => response.json())
	        .then(data => {
	        	console.log(data);
				this.props.history.go(0);
	        });

	 // json-server
		// const requestOptions = {
	 //      method: 'GET',
	 //      headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' }
	 //    };
	 //    fetch(path.url + '/hosts/' + this.state.meet_id, requestOptions)
	 //        .then(response => response.json())
	 //        .then(data => {
	 //        	console.log(data);
	 //        });	 


		this.handleClose();
	}

	handleClose(){
		this.setState(state => ({
      		show: false,
      		setshow: false
    	}));
	}

	handleShow(){
		this.setState(state => ({
      		show: true,
      		setshow: true
    	}));
	}

	render(){
		return(
			<div>
				<Dropdown as={ButtonGroup} style = {{width: "100%"}} >
  				<Button variant="info" onClick={this.goMeeting} style = {{backgroundColor : this.state.voted ? "#ED9E50": "#8CBDB9"}} className = "act" block>
  					{this.state.title}
  				</Button>
  				<Dropdown.Toggle split variant="info" id="dropdown-split-basic" style = {{backgroundColor : this.state.voted ? "#ED9E50": "#8CBDB9"}} className = "act" />
				<Dropdown.Menu>
					<Dropdown.Item onClick={this.goUpdate}>
    					<GearFill className = "dropdown_icon" />
    					update
    				</Dropdown.Item>
					{this.state.voted ? "":
    					<Dropdown.Item onClick={this.goDecision}>
    						<Tools className = "dropdown_icon" />
    						finish vote
    					</Dropdown.Item>
					}
					<Dropdown.Item onClick={this.handleShow}>
						<TrashFill className = "dropdown_icon" />
						delete
					</Dropdown.Item>
  				</Dropdown.Menu>
				</Dropdown>
				<Modal animation={false} show={this.state.show} onHide={this.handleClose} className = "modal">
			        <Modal.Header closeButton>
			          <Modal.Title>Do You Want to Delete this Acitivity?</Modal.Title>
			        </Modal.Header>
			        <Modal.Footer>
			          <Button variant="secondary" onClick={this.handleClose} className = "modal_no">
			            No
			          </Button>
			          <Button variant="primary" onClick={this.goDelete} className = "modal_yes">
			            Yes
			          </Button>
			        </Modal.Footer>
			    </Modal>
			</div>
		);
	}
}

export default ActEdit;
