import React from 'react';
import ActEdit from './ActEdit';
import path from '../config';
import './css/basic.css';

class Host extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			acti_edit: []
		}

	    const requestOptions = {
	      method: 'GET',
	      headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
	    };
	    fetch(path.url + '/users/' + localStorage.getItem('user_id') + '/meetings?host=true', requestOptions)
	        .then(response => response.json())
	        .then(data => {
	        	var temp = [];
	        	for (var i = 0; i < data.length; i++) {
	        		var voted = false;
	        		if(data[i].final_slots.length !== 0){
	        			voted = !voted;
	        		}
					temp.push(<ActEdit key = {data[i].hash_id} meet_id = {data[i].hash_id} title = {data[i].title} 
								voted = {voted} history = {this.props.history} />)
				}
				this.setState({ acti_edit: temp })
	        });
	}

	render(){
		return(
				<div className = "list">
					{this.state.acti_edit}
				</div>
		);
	}
}

export default Host;
