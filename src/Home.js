import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import NavBar from './component/NavBar';
import ActList from './component/ActList';
import CalenderHome from './component/CalenderHome';
import './component/css/basic.css';

class Home extends React.Component{
	render(){
		return(
			<div>
			<NavBar history = {this.props.history} />
    			<Container className = "con">
      			  <Row>
	        		<Col>
	        			<ActList history ={this.props.history} />
	        		</Col>
	        		<Col>
	          			<CalenderHome />
	        		</Col>
    		  	  </Row>
				</Container>
			</div>
		);
	}
}

export default Home;