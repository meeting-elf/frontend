import React from 'react';
import NavBar from './component/NavBar';
import { Container, Row, Col, Button, Modal } from 'react-bootstrap';
import Calender from './component/Calender';
import CalenderVote from './component/CalenderVote';
import './component/css/basic.css';
import path from './config';

class Decision extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			title: "name",
			description: "hello",
			location: "EC122",
			meeting_link: "www.google.com",
			vote_slots: [],
			final_slots: [],
			max_vote: 0,
			render: false
		}

		const req = {
			method: 'GET',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   'Accept-Control-Allow-Origin': path.url
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
		};

		fetch(path.url + '/meetings/' + this.props.match.params.hash_id , req)
			.then(response => response.json())
			.then(data => {
				var temp = []
				// backend
				for(var i = 0;i < data.vote_slots.length;i++){
					temp.push(data.vote_slots[i]);
				}
				this.setState({
					title: data.title,
					description: data.description,
					location: data.location,
					meeting_link: data.meeting_link,
					vote_slots: temp,
					final_slots: data.final_slots,
					max_vote: data.max_vote,
					render: true
				});
			});
	}

	render(){
		console.log(this.state.vote_slots);
		if(this.state.render){
			return(
				<div>
					<NavBar history = {this.props.history} />
					<h1 className="page_title">{this.state.title}</h1>
					<h6 className = "description">{this.state.description}</h6>

					<Container>
					  <h6 className = "context">Location: {this.state.location}</h6>
					  <h6 className = "context">Meeting Link: {this.state.meeting_link}</h6>
					  <Row>
					    <Col>
					    	<CalenderVote hash_id = {this.props.match.params.hash_id} mode = "final_vote" history = {this.props.history} voted_slots = {this.state.final_slots} />
					    </Col>
					    <Col>
					    	<Calender vote_slots = {this.state.vote_slots} max_vote = {this.state.max_vote} />
					    </Col>
					  </Row>
					</Container>
				</div>
			);
		}else{
			return null;
		}
	}
}

export default Decision;
