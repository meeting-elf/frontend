import React from 'react';
import { Link } from 'react-router-dom';
// import { withCookies } from 'react-cookie';
import { Navbar, Button, Form, Container, Row, Col } from 'react-bootstrap';
import './component/css/basic.css';
import path from './config';

class Login extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			email: "",
			password: ""
		}
		this.SendData = this.SendData.bind(this);
	}

	SendData(){
		// backend
		const requestOptions = {
        	method: 'POST',
        	headers: { 
        		'Content-Type': 'application/json', 
        		'Accept': 'application/json',
        		'Accept-Control-Allow-Origin': path.url,
        		'Accept-Control-Allow-Credentials': true,
        	},
        	credentials: 'include',
            withCredentials: true,
        	body: JSON.stringify({ "email": this.state.email, 
        						   "password": this.state.password })
        };

    	fetch(path.url + '/auth/login', requestOptions)
        	.then(response => response.json())
        	.then(data => {
        		// console.log(data);
        		localStorage.setItem('user_id', data.id);
       			this.props.history.push({
				    pathname:'/home'
			    })
			});

		// json-server
		// localStorage.setItem('user_id', 1);
		// this.props.history.push({
		// 	pathname:'/home',
		// })
	}

	render(){
		return(
			<div>
			  <Navbar expand = "lg" className = "bar">
    			<Navbar.Brand className = "brand" href="/">Meetingelf</Navbar.Brand>
  			  </Navbar>
			  <Container className = "con">
  				<Row>
    				<Col></Col>
    				<Col xs={6}>
    					<h1 className = "title">Login</h1>
    					<Form>
			  				<Form.Group controlId="formBasicEmail" onChange={e => this.setState({ email: e.target.value })}>
			    			  <Form.Label className = "form_text">Email</Form.Label>
			    			  <Form.Control type="email" placeholder="Enter your email" className = "form_input" />
			  				</Form.Group>

			  				<Form.Group controlId="formBasicPassword" onChange={e => this.setState({ password: e.target.value })}>
			    			  <Form.Label className = "form_text">Password</Form.Label>
			    			  <Form.Control type="password" placeholder="Enter your password" className = "form_input" />
			  				</Form.Group>
			  			    <div className = "div_btn">
			  				  <Button variant="primary" className = "submit_btn" onClick={this.SendData}>Login</Button>{' '}
							</div>
						</Form>
						<h5 className = "si_block">
						  No account?
						  <Link to = '/signup' className = "sig_link"> Create one</Link>
						</h5>
    				</Col>
    				<Col></Col>
  				</Row>
  			  </Container>
			</div>
		);
	}
}

export default Login;
