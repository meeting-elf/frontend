import React from 'react';
import NavBar from './component/NavBar';
import './component/css/basic.css';
import { Row, Col, Form, Container, Button } from 'react-bootstrap';
import path from './config';

class Create extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: "New meeting",
			description: "Create a new meeting",
			meeting_link: "www.google.com",
			location: "EC122",
			start_hour: "08",
			end_hour: "12"
		}
		this.SendData = this.SendData.bind(this);
	}

	SendData() {
		// backend
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
			body: JSON.stringify({
				// "token": localStorage.getItem('user_id'),
				"title": this.state.title,
				"description": this.state.description,
				"meeting_link": this.state.meeting_link,
				"location": this.state.location,
				"start_hour": this.state.start_hour,
				"end_hour": this.state.end_hour
			})
		};

		fetch(path.url + '/meetings', requestOptions)
			.then(response => response.json())
			.then(data => {
				// console.log(data);
				this.props.history.push({
					pathname: '/home'
				})
			});

		// json-server
		// meetings
		// const requestOptions = {
		// 	method: 'POST',
		// 	headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
		// 	body: JSON.stringify({
		// 		"userId": 1,
		// 		"hostId": 1,
		// 		"vote": false,
       	//		"hash_id": "AAAAAAF",
		// 		"hostname": "Elf111",
		// 		"final_slots": [],
		// 		"mode": "weekly",
		// 		"title": this.state.name,
		// 		"description": this.state.description,
		// 		"meeting_link": this.state.link,
		// 		"location": this.state.location,
		// 		"start_hour": this.state.start,
		// 		"end_hour": this.state.end
		// 	})
		// };

		// fetch(path.url + '/meetings', requestOptions)
		// 	.then(response => response.json())
		// 	.then(data => {
		// 		// console.log(data);
		// 		// hosts
		// 		const req - {
		// 			method: 'POST',
		// 			headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
		// 			BODY: JSON.stringify({
		// 				""
		// 			})
		// 		}
		// 		this.props.history.push({
		// 			pathname: '/home'
		// 		})
		// 	});
	}

	render() {
		return (
			<div>
				<NavBar history={this.props.history} />
				<h1 className="page_title"> Create an Activity</h1>
				<Container>
					<Row>
						<Col>
							<Form>
								<Form.Group controlId="formBasicName" onChange={e => this.setState({ title: e.target.value })}>
									<Form.Label className="form_text">Name</Form.Label>
									<Form.Control type="name" className="form_input" />
								</Form.Group>
								<Form.Group controlId="formBasicDescription" onChange={e => this.setState({ description: e.target.value })}>
									<Form.Label className="form_text">Description</Form.Label>
									<Form.Control className="form_input" as="textarea" rows={6} />
								</Form.Group>
								<Form.Group controlId="formBasicLocation" onChange={e => this.setState({ location: e.target.value })}>
									<Form.Label className="form_text">Location</Form.Label>
									<Form.Control type="location" className="form_input" />
								</Form.Group>
								<Form.Group controlId="formBasicLink" onChange={e => this.setState({ link: e.target.value })}>
									<Form.Label className="form_text">Meeting Link</Form.Label>
									<Form.Control type="link" className="form_input" />
								</Form.Group>
							</Form>
						</Col>
						<Col>
							<Form.Row>
								<Form.Group as={Col} controlId="formStart" onChange={e => this.setState({ start: e.target.value })}>
									<Form.Label className="form_text">Start hour</Form.Label>
									<Form.Control type="start" className="form_input" />
								</Form.Group>

								<Form.Group as={Col} controlId="formGridEnd" onChange={e => this.setState({ end: e.target.value })}>
									<Form.Label className="form_text">End hour</Form.Label>
									<Form.Control type="end" className="form_input" />
								</Form.Group>
							</Form.Row>
							<div className="div_btn_right">
								<Button variant="primary" className="submit_btn" onClick={this.SendData}>Create</Button>{' '}
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Create;