import React from 'react';
import {
	BrowserRouter as Router,
	// Router,
	Switch,
	Route
} from "react-router-dom";
// import { CookiesProvider } from 'react-cookie';
// import { withCookies } from 'react-cookie';
import Home from './Home';
import Login from './Login';
import Signup from './Signup';
import Meeting from './Meeting';
import Create from './Create';
import Update from './Update';
import Vote from './Vote';
import Decision from './Decision';
import path from './config';
// import { createBrowserHistory } from "history";

// <Route path = "/" exact render={() => (<Login cookies={this.props.cookies} history = {this.props.history} />)} />
// <Route path = "/" exact component = {Login} />
// <CookiesProvider>

// const customHistory = createBrowserHistory();

class MyRouter extends React.Component{
	render(){
		return(
			<Router basename = {path.root_dir}>
				<Switch>
					<Route path = "/meeting/:hash_id/vote" component = {Vote} />
					<Route path = "/meeting/:hash_id/decision" component = {Decision} />
					<Route path = "/meeting/:hash_id/update" component = {Update} />
					<Route path = "/meeting/:hash_id" component = {Meeting} />
					<Route path = "/home" exact component = {Home} />
					<Route path = "/signup" component = {Signup} />
					<Route path = "/create" component = {Create} />
					<Route path = "/" exact component = {Login} />
				</Switch>
			</Router>
		);
	}
}

export default MyRouter;