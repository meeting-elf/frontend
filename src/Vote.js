import React from 'react';
import NavBar from './component/NavBar';
import { Container, Row, Col, Button, Modal } from 'react-bootstrap';
import Calender from './component/Calender';
import CalenderVote from './component/CalenderVote';
import './component/css/basic.css';
import path from './config';



class Vote extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			title: "name",
			description: "hello",
			location: "EC122",
			meeting_link: "www.google.com",
			own_vote_slots: [],
			vote_slots: [],
			max_vote: 0,
			render: false,
			show: false,
			setshow: false
		}

		this.goParticipate = this.goParticipate.bind(this);

		const req1 = {
		    method: 'GET',
		    headers: { 'Content-Type': 'application/json', 
			       'Accept': 'application/json'
		    },
		};
		fetch(path.url + '/auth/status', req1)
			.then(response => {
				response.json();
				if(response.status == 200){
				const requestOptions = {
				    method: 'GET',
				    headers: { 'Content-Type': 'application/json', 
				   		'Accept': 'application/json'
				    },
				};
				fetch(path.url + '/meetings/' + this.props.match.params.hash_id + '/participate', requestOptions)
				    .then(response => response.json())
				    .then(data => {
					this.setState({
					    show: !data.participate,
					    setshow: !data.participate
				        });
			            });
			    }
			})
			.then(data => {});

		const req = {
			method: 'GET',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   'Accept-Control-Allow-Origin': path.url
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
		};

		fetch(path.url + '/meetings/' + this.props.match.params.hash_id , req)
			.then(response => response.json())
			.then(data => {
				var temp = []
				// backend
				for(var i = 0;i < data.vote_slots.length;i++){
					temp.push(data.vote_slots[i]);
				}
				this.setState({
					title: data.title,
					description: data.description,
					location: data.location,
					meeting_link: data.meeting_link,
					vote_slots: temp,
					max_vote: data.max_vote
				});

				const req2 = {
					method: 'GET',
					headers: { 'Content-Type': 'application/json', 
							   'Accept': 'application/json'
					}
				};
				
				fetch(path.url + '/meetings/' + this.props.match.params.hash_id + '/vote', req2)
					.then(response => response.json())
					.then(data => {
						if(data.vote){
							this.setState({own_vote_slots: data.vote_slots});
						}
						this.setState({render: true});
					});
			});
	}

	goParticipate(){
		const requestOptions = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 
					   'Accept': 'application/json',
					   // 'Accept-Control-Allow-Origin': path.url,
                 	   // 'Accept-Control-Allow-Credentials': true,
			},
			// credentials: 'include',
      		// withCredentials: true,
			body: JSON.stringify({
			})
		};
		
		fetch(path.url + '/meetings/' + this.props.match.params.hash_id + '/participate', requestOptions)
			.then(response => response.json())
			.then(data => {

			});

	this.handleClose();
	}

	handleClose(){
		this.setState(state => ({
      		show: false,
      		setshow: false
    	}));
	}

	handleShow(){
		this.setState(state => ({
      		show: true,
      		setshow: true
    	}));
	}

	render(){
		var Parti = this.state.show;
		if(this.state.render){
			return(
				<div>
					<NavBar history = {this.props.history} />
					<h1 className="page_title">{this.state.title}</h1>
					<h6 className = "description">{this.state.description}</h6>

					<Container>
					{Parti ?
						<Button variant="primary" onClick={this.goParticipate} className = "modal_yes">
							Participate
						</Button>
					:
					null
					}
					  <h6 className = "context">Location: {this.state.location}</h6>
					  <h6 className = "context">Meeting Link: {this.state.meeting_link}</h6>
					  <Row>
					    <Col>
					    	<CalenderVote hash_id = {this.props.match.params.hash_id} mode = "vote" voted_slots = {this.state.own_vote_slots} history = {this.props.history}/>
					    </Col>
					    <Col>
					    	<Calender vote_slots = {this.state.vote_slots} max_vote = {this.state.max_vote} />
					    </Col>
					  </Row>
					</Container>
				</div>
			);
		}else{
			return null;
		}
	}
}

export default Vote;
